import 'package:flutter/material.dart';
import 'app_layout.dart';

class VideoPlayer extends StatelessWidget {
  const VideoPlayer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
   
    final isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return OrientationBuilder(builder: (context, orientation) {
      return isPortrait
          ? Column(
              children: [
                AspectRatio(
                  aspectRatio: 16 / 9,
                  child: Container(
                    color: Colors.black,
                    child: const Center(
                      child: Icon(
                        Icons.play_circle,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                const VideoDescription(),
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AspectRatio(
                  aspectRatio: 1 / 6,
                ),
                Expanded(
                  child: Container(
                    color: Colors.black,
                    child: const Center(
                      child: Icon(
                        Icons.play_circle,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                AspectRatio(
                  aspectRatio: 1 / 6,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FloatingActionButton.small(
                          onPressed: () {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                content: const VideoDescription(),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'OK'),
                                    child: const Text(
                                      'close',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                          child: Icon(Icons.info),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            );
    });
  }
}

class VideoDescription extends StatelessWidget {
  const VideoDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: const [
          Text(
            'Video Title',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 24),
          ),
          Text(
            'Video description',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 18),
          ),
          Text(
            'Likes: 34',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }
}
